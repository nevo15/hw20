﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Text.RegularExpressions;



namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {

            InitializeComponent();
            this.Enabled = false;
            Thread StartTheGame = new Thread(gMassege);
            StartTheGame.Start();
           

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            Image myimage = new Bitmap(@"carddd.jpg");
            this.BackgroundImage = myimage;
        }


        private void GenerateCards(NetworkStream clientStream)
        {
            int x = 50, y = 550;

            Random r;
            int index;
            string randomCard;

            Point LocationRedC = new Point(x, y);
            Point LocationBlueC = new Point(624, 100);
            Point LocationRandomC = new Point(615, 50);
            Point LocationButton = new Point(620, 350);
            Point ScoreLocation = new Point(50, 60);
            Point SomeoneScoreLocation = new Point(1200, 60);

            var CardList = new List<string> { "_10_of_clubs", "_10_of_hearts", "_10_of_spades", "_2_of_diamonds", "_2_of_hearts", "_2_of_spades", "_3_of_clubs", "_3_of_diamonds", "_3_of_hearts", "_3_of_spades", "_4_of_clubs", "_4_of_diamonds", "_4_of_hearts", "_4_of_spades", "_5_of_clubs", "_5_of_diamonds", "_5_of_hearts", "_5_of_spades", "_6_of_clubs", "_6_of_diamonds", "_6_of_hearts", "_6_of_spades", "_7_of_clubs", "_7_of_diamonds", "_7_of_hearts", "_7_of_spades", "_8_of_clubs", "_8_of_diamonds", "_8_of_hearts", "_8_of_spades", "_9_of_clubs", "_9_of_diamonds", "_9_of_hearts", "_9_of_spades", "ace_of_clubs", "ace_of_diamonds", "ace_of_hearts", "ace_of_spades2", "jack_of_clubs2", "jack_of_diamonds2", "jack_of_hearts2", "jack_of_spades2", "king_of_clubs2", "king_of_diamonds2", "king_of_hearts2", "king_of_spades2", "queen_of_clubs2", "queen_of_diamonds2", "queen_of_hearts2", "queen_of_spades2", "_10_of_diamonds", "_2_of_clubs" };

            Dictionary<string, string> Dcard = new Dictionary<string, string>();

            Dcard.Add("ace_of_clubs", "101C");
            Dcard.Add("ace_of_diamonds", "101D");
            Dcard.Add("ace_of_hearts", "101H");
            Dcard.Add("ace_of_spades2", "101S");
            Dcard.Add("_2_of_diamonds", "102D");
            Dcard.Add("_2_of_hearts", "102H");
            Dcard.Add("_2_of_spades", "102S");
            Dcard.Add("_2_of_clubs", "102C");
            Dcard.Add("_3_of_clubs", "103C");
            Dcard.Add("_3_of_diamonds", "103D");
            Dcard.Add("_3_of_hearts", "103H");
            Dcard.Add("_3_of_spades", "103S");
            Dcard.Add("_4_of_clubs", "104C");
            Dcard.Add("_4_of_diamonds", "104D");
            Dcard.Add("_4_of_hearts", "104H");
            Dcard.Add("_4_of_spades", "104S");
            Dcard.Add("_5_of_clubs", "105C");
            Dcard.Add("_5_of_diamonds", "105D");
            Dcard.Add("_5_of_hearts", "105H");
            Dcard.Add("_5_of_spades", "105S");
            Dcard.Add("_6_of_clubs", "106C");
            Dcard.Add("_6_of_diamonds", "106D");
            Dcard.Add("_6_of_hearts", "106H");
            Dcard.Add("_6_of_spades", "106S");
            Dcard.Add("_7_of_clubs", "107C");
            Dcard.Add("_7_of_diamonds", "107D");
            Dcard.Add("_7_of_hearts", "107H");
            Dcard.Add("_7_of_spades", "107S");
            Dcard.Add("_8_of_clubs", "108C");
            Dcard.Add("_8_of_diamonds", "108D");
            Dcard.Add("_8_of_hearts", "108H");
            Dcard.Add("_8_of_spades", "108S");
            Dcard.Add("_9_of_clubs", "109C");
            Dcard.Add("_9_of_diamonds", "109D");
            Dcard.Add("_9_of_hearts", "109H");
            Dcard.Add("_9_of_spades", "109S");
            Dcard.Add("_10_of_clubs", "110C");
            Dcard.Add("_10_of_hearts", "110H");
            Dcard.Add("_10_of_spades", "110S"); 
            Dcard.Add("_10_of_diamonds", "110D");
            Dcard.Add("jack_of_clubs2", "111C");
            Dcard.Add("jack_of_diamonds2", "111D");
            Dcard.Add("jack_of_hearts2", "111H");
            Dcard.Add("jack_of_spades2", "111S");
            Dcard.Add("queen_of_clubs2", "112C");
            Dcard.Add("queen_of_diamonds2", "112D");
            Dcard.Add("queen_of_hearts2", "112H");
            Dcard.Add("queen_of_spades2", "112S");
            Dcard.Add("king_of_clubs2", "113C");
            Dcard.Add("king_of_diamonds2", "113D");
            Dcard.Add("king_of_hearts2", "113H");
            Dcard.Add("king_of_spades2", "113S");


            

            //creat 10 red card
            for (int i = 0; i < 10; i++)
            {
                // create the image object itself
                System.Windows.Forms.PictureBox RedCard = new PictureBox();
                RedCard.Name = "picDynamic" + i; // in our case, this is the only property that changes between the different images
                RedCard.Image = global::WindowsFormsApplication1.Properties.Resources.card_back_red;
                RedCard.Location = LocationRedC;
                RedCard.Size = new System.Drawing.Size(110, 150);
                RedCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                LocationRedC.X += x + 80;



                // assign an event to it
                RedCard.Click += delegate(object sender1, EventArgs e1)
                {
                    r = new Random();
                    index = r.Next(CardList.Count);
                    randomCard = CardList[index];

                    //send to the server
                    byte[] buffer = new ASCIIEncoding().GetBytes(Dcard[randomCard]);
                    clientStream.Write(buffer, 0, 4);
                    clientStream.Flush();

                   int getNumberRandomCar =  int.Parse(Regex.Match(Dcard[randomCard], @"\d+", RegexOptions.RightToLeft).Value);

                    System.Windows.Forms.PictureBox Randomc = new PictureBox();
                    object O = global::WindowsFormsApplication1.Properties.Resources.ResourceManager.GetObject(randomCard);
                    ((PictureBox)sender1).Image = (Image)O;
                    ((PictureBox)sender1).Size = new System.Drawing.Size(110, 159);
                    ((PictureBox)sender1).SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                    this.Controls.Add(((PictureBox)sender1));

                    gMassegeFromServer(clientStream, getNumberRandomCar);
                };
                this.Controls.Add(RedCard);
            }


            //creat BlueCard
            System.Windows.Forms.PictureBox BlueCard = new PictureBox();
            BlueCard.Name = "picDynamic";
            BlueCard.Image = global::WindowsFormsApplication1.Properties.Resources.card_back_blue;
            BlueCard.Location = LocationBlueC;
            BlueCard.Size = new System.Drawing.Size(110, 150);
            BlueCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Controls.Add(BlueCard);

            //creat butten to quit
            System.Windows.Forms.Button foldet = new Button();
            foldet.Text = "FOLDET";
            foldet.Location = LocationButton;
            foldet.Size = new System.Drawing.Size(115, 38);
            this.Controls.Add(foldet);

            //creat "your score"

            System.Windows.Forms.Label score = new Label();
            score.Name = "yourSource";
            score.Text = "Your Source: ";
            score.Size = new System.Drawing.Size(100, 15);
            score.Location = ScoreLocation;
            this.Controls.Add(score);

            System.Windows.Forms.Label SomeoneScore = new Label();
            SomeoneScore.Name = "competitorSource";
            SomeoneScore.Text = "Competitor Source: ";
            SomeoneScore.Size = new System.Drawing.Size(110, 15);
            SomeoneScore.Location = SomeoneScoreLocation;
            this.Controls.Add(SomeoneScore);

        }

        //wait for massege
        public void gMassege()
        {
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            client.Connect(serverEndPoint);
            NetworkStream clientStream = client.GetStream();
            byte[] bufferIn = new byte[4];
            int bytesRead = clientStream.Read(bufferIn, 0, 4);
            string input = new ASCIIEncoding().GetString(bufferIn);
            this.BeginInvoke((Action)delegate()
            {
                this.Enabled = true;
                this.GenerateCards(clientStream);
            });

        }

        public void gMassegeFromServer(NetworkStream clientStream,int RandomNumC  )
        {
            byte[] bufferIn = new byte[4];
            int bytesRead = clientStream.Read(bufferIn, 0, 4);
            string input = new ASCIIEncoding().GetString(bufferIn);

            int i = 0;
            int j = 0;

            int NumC = int.Parse(Regex.Match(input, @"\d+", RegexOptions.RightToLeft).Value);


           if(RandomNumC < NumC)
           {
               i += 1; 
               SomeoneScore.Text = "Competitor Source: " + i;
               this.Controls.Add(SomeoneScore);
           }
           else if (RandomNumC > NumC)
           {
               j += 1;
               score.Text = "Competitor Source: " + i;
               this.Controls.Add(score);
           }

    

        }

        public Control SomeoneScore { get; set; }

        public Control score { get; set; }
    }
}









